# Installation

Pour ce cours d'introduction à la programmation, vous avez besoin
d'un ordinateur avec les caractéristiques suivantes:

* Microsoft Windows 10/8/7/Vista/2003/XP, Apple macOS 10.8 (Mountain Lion)
  ou supérieur, ou GNU/Linux avec GNOME ou KDE desktop.
* Au minimum 1 GB de mémoire vive (RAM); recommandé 2 GB.
* Un disque dur (ou SSD) avec au minimum 500 MB d'espace libre pour installer
  les programmes et au minimum 1 GB pour les caches.
* Un écran avec une résolution minimum de 1024x768.

## Java

Kotlin génère du code pour la machine virtuelle «Java» et pour exécuter vos programmes, vous aurez besoin de Java. Vous pouvez le
[télécharger](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html) gratuitement depuis le site Internet de java.
Pour ce cours, nous utiliserons la version 11 de java.

## JetBrain IntelliJ IDEA Community Edition

Pour éditer les programmes, nous utiliserons «IntelliJ IDEA Community Edition». Vous pouvez
[télécharger](https://www.jetbrains.com/idea/download/) gratuitement
ce logiciel depuis le site de JetBrain.

## Kotlin

Le langage [Kotlin](https://kotlinlang.org/) est inclus dans l'environnement de développement «IntelliJ IDEA Community Edition». Vous trouverez une
[documentation](https://kotlinlang.org/docs/reference/) détaillée sur Internet.